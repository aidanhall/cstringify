#pragma once
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#define STDIO_MARKER "-"


#define INPUT_NAME_POS (1)
#define INPUT_FILE_POS (INPUT_NAME_POS+1)
#define OUTPUT_FILE_POS (INPUT_FILE_POS+1)

#define DEFAULT_CHAR_TYPE "char"

/* Creates a char_type * called string_name, reading source from input and writing the result to output. */
int cstringify( const char* string_name, const char* char_type, FILE* input, FILE* output);
/* A wrapper to cstringify() that attempts to open files based on their names. */
int cstringify_name( const char* string_name, const char* char_type, char* input_name, char* output_name );
// vim: ft=c
