# CStringify
A tiny utility to convert files to C strings.

## Rationale
I don't want to worry about not having xxd one day.

## Inspiration
https://stackoverflow.com/a/19639278

## Usage
If input or output filename is -, stdio will be used.
```sh
$ cstringify varname inputfile outputfile [char type]
```

# Library and SWIG
I have used this tiny library to teach myself how to use SWIG.
Bindings for Python 3 and TCL 8 can be automatically produced, if possible.

The library contains:
* `cstringify()`:
    * It parses an input FILE and sends the result to the output FILE.
    * FILE*'s can't be created outside C code, so this would only by useful (and is only accessible) when using the library in C.
* `cstringify_name()`:
    * This is a wrapper on `cstringify()` which takes filename strings.
    * Its arguments are all 'primitive' strings, so it can easily be called anywhere, including from another language via a SWIG-generated interface.
    * It contains most of the code that was previously in the `main()` function of the executable.
    * Separating this functionality from the executable benefits the library's usability, and makes the executable program's code tiny, at just 18 lines including blank lines and comments.
## Python:
```sh
$ cd build/python
$ python3
```
```python
>>> import cstringify
>>> help(cstringify)
>>> cstringify.cstringify_name("frag_shader", "char", "frag.glsl", "frag-glsl.h")
```
## TCL 8:
```sh
$ cd build/tcl8
$ tclsh
```
```tcl
% load ./cstringify.so
% cstringify_name frag_shader char frag.glsl frag-glsl.h
```
