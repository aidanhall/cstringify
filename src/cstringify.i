%module cstringify
%{
#include <cstringify.h>
extern int cstringify_name( const char* string_name, const char* char_type, char* input_name, char* output_name );
%}

extern int cstringify_name( const char* string_name, const char* char_type, char* input_name, char* output_name );
