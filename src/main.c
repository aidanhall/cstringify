#include "cstringify.h"

int main(int argc, char* argv[]) {
    // Args.
    if (argc < (OUTPUT_FILE_POS+1)) {
	fprintf(stderr, "Usage: %s varname inputfile outputfile [list_type]\n", argv[0]);
	return 1;
    }

    // List type
    char* list_type = NULL;
    if (argc > (OUTPUT_FILE_POS+1)) {
	list_type = argv[OUTPUT_FILE_POS+1];
    }

    int result = cstringify_name(argv[INPUT_NAME_POS], list_type, argv[INPUT_FILE_POS], argv[OUTPUT_FILE_POS]);
    return result;
}
