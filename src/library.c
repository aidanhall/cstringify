#include "cstringify.h"

#define PRAGMA_ONCE "#pragma once\n"

int cstringify(const char* string_name, const char* char_type, FILE* input, FILE* output) {

    // Start.

    // memory leak but i'm lazy

    if (string_name == NULL || input == NULL || output == NULL) {
	return 1;
    }

    fputs(PRAGMA_ONCE, output);
    fprintf(output, "const %s * %s = \n",
	    (char_type == NULL)? DEFAULT_CHAR_TYPE :
	    (strcmp(char_type, "") == 0)? DEFAULT_CHAR_TYPE :char_type,
	    string_name);

    // Lines.
    char linebuffer [BUFSIZ];
    size_t buffer_size;
    size_t char_count = 0;

    while(fgets(linebuffer, BUFSIZ, input)) {
	fputc('"', output);
	int char_idx;
	for (char_idx = 0; linebuffer[char_idx] != EOF && linebuffer[char_idx] != '\n'; ++char_idx) {
	    switch (linebuffer[char_idx]) {
		case '"':
		case '%':
		    // Escape required characters.
		    fputc('\\', output);
		    fputc(linebuffer[char_idx], output);
		    break;
		default:
		    fputc(linebuffer[char_idx], output);
		    break;
	    }
	}
	char_count += char_idx + 1; // Adds the number of characters in the line + the newline.
	fputs("\\n\"\n", output);
    }

    char_count += 1; // NULL-terminating byte.

    fprintf(output,
	    "\"\\0\";\n"
	    "const size_t "
	    "%s"
	    "_len = %lu;\n",
	    string_name, char_count);
    return 0;
}

int cstringify_name( const char* string_name, const char* char_type, char* input_name, char* output_name ) {

    // Files.
    FILE* inputfile = NULL;
    FILE* outputfile = NULL;

    // Input file.
    if (strcmp(input_name, STDIO_MARKER) == 0) {
	inputfile = stdin;
    } else {
	inputfile = fopen(input_name, "r");
	if (inputfile == NULL) {
	    perror(input_name);
	    return 1;
	}
    }

    // Output file.
    if (strcmp(output_name, STDIO_MARKER) == 0) {
	outputfile = stdout;
    } else {
	outputfile = fopen(output_name, "w");
	if (outputfile == NULL) {
	    perror(output_name);
	    return 1;
	}
    }


    errno = 0;


    cstringify(string_name, char_type, inputfile, outputfile);

    fclose(inputfile);
    fclose(outputfile);
    return 0;
}
